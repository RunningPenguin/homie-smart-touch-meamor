/*
 * RELAISNode.hpp
 * Homie node for a Relais
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#pragma once

#include <Homie.hpp>
#include "HardwareConfig.h"



class RELAISNode : public HomieNode
{
private:
  const char *cCaption = "• Relais output:";

  // Settable properties
  const char *cPropOn1 = "on1"; // Not really settable, always reports true, because the controller is connected to a physical switch
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  const char *cPropOn2 = "on2"; // Not really settable, always reports true, because the controller is connected to a physical switch
#endif
#if defined THREE_BUTTONS
  const char *cPropOn3 = "on3"; // Not really settable, always reports true, because the controller is connected to a physical switch
#endif

  uint32_t LastHighIn;

  //the amount of milliseconds the sensor has to be low 
  //before we assume all motion has stopped
  uint32_t pause = 5000;

  boolean lockLow = true;
//   boolean takeLowTime;
  boolean allowMotionDetection = true;

protected:
  virtual bool handleInput(const HomieRange &range, const String &property, const String &value);
  virtual void loop() override;

public:
  RELAISNode(const char *id, const char *name);

  void beforeSetup();
  void setup();
  void returnStatusToOpenhab();

};