/*
 * RELAISNode.hpp
 * Homie node for a cheap chinese PIR Motion sensor
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#include <RELAISNode.hpp>



RELAISNode::RELAISNode(const char *id, const char *name)
    : HomieNode(id, name, "Relais Output")
{
}


bool RELAISNode::handleInput(const HomieRange &range, const String &property, const String &value)
{
  if (property == cPropOn1)
  {
    if (value == "true")
    {
        digitalWrite(RELAIS_OUTPUT_PIN1, HIGH);
        setProperty(cPropOn1).send("true");
    }
    if (value == "false")
    {
        digitalWrite(RELAIS_OUTPUT_PIN1, LOW);
        setProperty(cPropOn1).send("false");
    }
  }
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  else if (property == cPropOn2)
  {
    if (value == "true")
    {
        digitalWrite(RELAIS_OUTPUT_PIN2, HIGH);
        setProperty(cPropOn2).send("true");
    }
    if (value == "false")
    {
        digitalWrite(RELAIS_OUTPUT_PIN2, LOW);
        setProperty(cPropOn2).send("false");
    }
  }
#endif
#if defined THREE_BUTTONS
  else if (property == cPropOn3)
  {
    if (value == "true")
    {
        digitalWrite(RELAIS_OUTPUT_PIN3, HIGH);
        setProperty(cPropOn3).send("true");
    }
    if (value == "false")
    {
        digitalWrite(RELAIS_OUTPUT_PIN3, LOW);
        setProperty(cPropOn3).send("false");
    }
  }
#endif
  return true;
}


void RELAISNode::loop()
{

}


void RELAISNode::beforeSetup()
{
  advertise(cPropOn1).setName("on/off Relais 1").setDatatype("boolean").settable();         // on/off = true/false
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  advertise(cPropOn2).setName("on/off Relais 2").setDatatype("boolean").settable();         // on/off = true/false
#endif
#if defined THREE_BUTTONS
  advertise(cPropOn3).setName("on/off Relais 3").setDatatype("boolean").settable();         // on/off = true/false
#endif
}


void RELAISNode::setup()
{
  pinMode(RELAIS_OUTPUT_PIN1, OUTPUT);
  digitalWrite(RELAIS_OUTPUT_PIN1, LOW);

#if defined TWO_BUTTONS || defined THREE_BUTTONS
  pinMode(RELAIS_OUTPUT_PIN2, OUTPUT);
  digitalWrite(RELAIS_OUTPUT_PIN2, LOW);
#endif
#if defined THREE_BUTTONS
  pinMode(RELAIS_OUTPUT_PIN3, OUTPUT);
  digitalWrite(RELAIS_OUTPUT_PIN3, LOW);
#endif
}

void RELAISNode::returnStatusToOpenhab()
{
  if(digitalRead(RELAIS_OUTPUT_PIN1) == HIGH) setProperty(cPropOn1).send("true");
  if(digitalRead(RELAIS_OUTPUT_PIN1) == LOW) setProperty(cPropOn1).send("false");

#if defined TWO_BUTTONS || defined THREE_BUTTONS
  if(digitalRead(RELAIS_OUTPUT_PIN2) == HIGH) setProperty(cPropOn2).send("true");
  if(digitalRead(RELAIS_OUTPUT_PIN2) == LOW) setProperty(cPropOn2).send("false");
#endif

#if defined THREE_BUTTONS
  if(digitalRead(RELAIS_OUTPUT_PIN3) == HIGH) setProperty(cPropOn3).send("true");
  if(digitalRead(RELAIS_OUTPUT_PIN3) == LOW) setProperty(cPropOn3).send("false");
#endif
}