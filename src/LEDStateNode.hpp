/*
 * LEDStateNode.hpp
 * Homie node for a LEDstates
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#pragma once

#include <Homie.hpp>
#include "HardwareConfig.h"



class LEDStateNode : public HomieNode
{
private:
  const char *cCaption = "• LEDstates:";

  // Settable properties
  const char *cPropInvert1 = "ledinvert1"; // turns LED of Button 1 on/off
  const char *cPropBlink1 = "ledblink1"; // let the LED of Button 1 blink in the given frequency and the given amount of times
  const char *cPropBlinkTimes1 = "ledblinktimes1"; // gives the amount of times how often the LED of Button 1 should blink
  const char *cPropBlinkfrequency1 = "ledonblinkfrequency1"; // gives the frequency in which the LED of Button 1 should blink
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  const char *cPropInvert2 = "ledinvert2";
  const char *cPropBlink2 = "ledblink2";
  const char *cPropBlinkTimes2 = "ledblinktimes2";
  const char *cPropBlinkfrequency2 = "ledonblinkfrequency2";
#endif
#if defined THREE_BUTTONS
  const char *cPropInvert3 = "ledinvert3";
  const char *cPropBlink3 = "ledblink3";
  const char *cPropBlinkTimes3 = "ledblinktimes3";
  const char *cPropBlinkfrequency3 = "ledonblinkfrequency3";
#endif

  uint32_t LastHighIn;

  //the amount of milliseconds the sensor has to be low 
  //before we assume all motion has stopped
  // uint32_t pause = 2000;
  uint32_t MillisActivateReset[3] = {0, 0, 0};

  uint32_t blinkFrequency[3] = {1000, 1000, 1000};
  uint32_t blinkTimes[3] = {1, 1, 1};

  int32_t lockLowLed[3] = {1, 1, 1};
  bool ledState[3] = {true, true, true};
//   boolean takeLowTime;
  boolean allowAutoReset = true;


  // enum LEDINDEX // Enum for array indices
  // {
  //   BUTTON1 = 0,
  //   BUTTON2,
  //   BUTTON3
  // };

  int32_t tryStrToInt(const String &value, const int maxvalue = 100);


protected:
  virtual bool handleInput(const HomieRange &range, const String &property, const String &value);
  virtual void loop() override;
  void resetAfterShortTime();
  void returnLedOnOff1ToOpenhab();
  void returnLedOnOff2ToOpenhab();
  void returnLedOnOff3ToOpenhab();
  void returnBlink1ToOpenhab();
  void returnBlink2ToOpenhab();
  void returnBlink3ToOpenhab();

public:
  LEDStateNode(const char *id, const char *name);

  void beforeSetup();
  void setup();
  void returnStatusToOpenhab();

};